#!/bin/bash
# get the foodid from search for POST
searchfn="food-search-get.txt"
foodid=$(cat $searchfn |jq -r .parsed[].food.foodId)
# n.b.  -m 1 stop after 1st, -C 3 before & 3 after
grep -i -m 1 -C 3 foodid $searchfn
echo foodId $foodid

