package ca.campbell.httpexamplepost

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.ByteArrayOutputStream
import java.io.DataOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.io.UnsupportedEncodingException
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.URLEncoder
import java.security.cert.Certificate

import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLPeerUnverifiedException
import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView


/*
 * HttpURLConnectionExamplePOST
 *
 * This app sends a URL uses HttpURLConnection class + POST
 *
 * to download a JSON result and display some of it in a jsonResult
 * I don't parse the JSON, I leave that up to you
 * remember: object = {}  array = []
 *
 * The api used is api.edamam.com, using Java 6 org.json
 * http://www.json.org/java/
 * note GSON is nicer to use if you want
 * https://sites.google.com/site/gson/gson-user-guide
 * For the API info and key  check www.edamam.com
 * 	example
 * 	curl -d @recipe.json -H "Content-Type: application/json" "https://api.edamam.com/api/nutrition-details?app_id=${YOUR_APP_ID}&app_key=${YOUR_APP_KEY}"
 * 	test with
 * 	curl -d @recipe.json -H "Content-Type: application/json" "https://api.edamam.com/api/nutrition-details?app_id=${e6501390}&app_key=${71b785fe9091261dbe192db334d77dbd}"
 *
 * LAB-LAB-LAB-LAB:
 * If you are looking at this code in order to do the lab the following must be done:
 *
 * 1.  TODO Fix the Show Certs, it crashes, look in the log and discover why, then fix it!
 * 2.  TODO Deconstruct the JSON returned.  Using either GSON or the JSON classes
 *     and read the number of calories,
 *      isplay the number of calories and what ever other info you want.
 *
 * 3.  TODO (Optional)  the ingr is an array, modify the app to read several ingredients then get the returned info
 *     for that "recipe".
 */
class HttpsExamplePOST : Activity() {

    lateinit var foodIngr: EditText
    lateinit var jsonResult: TextView
    lateinit var ingr: TextView
    /*
	 * you may use these classes to build & deconstruct JSON
	private JSONObject foodJson;
	private JSONArray ingrJson;
	* or use GSON
	 */

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_http_example)

        foodIngr = findViewById(R.id.food) as EditText
        jsonResult = findViewById(R.id.jsonResult) as TextView
        ingr = findViewById(R.id.ingr) as TextView

        // check: encode only the parms, POST data and URL are ok
        /*
        try {
            urlString = (APIURL + "?extractOnly&app_id="
                    + URLEncoder.encode(APPID, "UTF-8")
                    + "&app_key=" + URLEncoder.encode(APPKEY, "UTF-8"))
        } catch (e: UnsupportedEncodingException) {
            jsonResult.text = "Unable to encode params for URL, contact developer"
            Log.e(TAG, e.message as String)
        }

        Log.d(TAG, "API URL: " + urlString!!)

         */
    }

    /*
     * When user clicks Load it Button, we use the data and
     * execute an AsyncTask to do the download. Before
     * attempting to fetch the URL, makes sure that there is a network
     * connection.
     */
    fun foodGETclickHandler(view: View) {
        Log.d(TAG, "requested GET food db")
        // Gets the data to look up  from thxe UI's text field.
        // this api requires ingr="string" via GET for the food request
        //urlString = APIURL

        val appidkey="app_id="+APPID+"&app_key="+APPKEY

        var foodInfo = foodIngr.text.toString()
        if (foodInfo.isEmpty())
            foodInfo = "1 tablespoon Butter"
        try {
            foodInfo = URLEncoder.encode(foodInfo, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            jsonResult.text = "Unable to encode params for URL, contact developer"
            Log.e(TAG, e.message as String)
        }
        var ingrdata  =  "ingr="+foodInfo

        ingr.text = "Food: $foodInfo"

        // first check to see if we can get on the network
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            // invoke the AsyncTask to do the dirty work.
            DownloadFoodData().execute(APIURL, ingrdata, "GET", appidkey)
        } else {
            jsonResult.text = "No network connection available."
        }
    } // clickHandler()
    fun nutrPOSTclickHandler(view: View) {
        // this api requires json data via post for the food request
        Log.d(TAG, "requested POST recipe")
        val appidkey="app_id="+APPIDN+"&app_key="+APPKEYN

        // first check to see if we can get on the network
        val connMgr = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            // invoke the AsyncTask to do the dirty work.
            DownloadFoodData().execute(APIURLN, RECIPE1_JSON, "POST", appidkey)
        } else {
            jsonResult.text = "No network connection available."
        }
        ingr.text = "Sending Recipe"
    }
    fun showNutrPOSTRecipe(view: View) {
        Log.d(TAG, "requested show recipe")
        jsonResult.text = RECIPE1_JSON
    }
    /*
	 * Uses AsyncTask to create a task away from the main UI thread. This task
	 * takes a URL string and uses it to create an HttpsUrlConnection. Once the
	 * connection has been established, the AsyncTask downloads the contents of
	 * the webpage via an an InputStream. The InputStream is converted into a
	 * string, which is displayed in the UI by the AsyncTask's onPostExecute
	 * method.
	 */

    private inner class DownloadFoodData : AsyncTask<String, Void, String>() {

        // onPreExecute log some info
        // runs in calling thread (in UI thread)
        override fun onPreExecute() {
            Log.d(TAG, "onPreExecute()")

        }

        // onPostExecute displays the results of the AsyncTask.
        // runs in calling thread (in UI thread)
        override fun onPostExecute(result: String) {
            // the json should be parsed, I am dumping it raw onto the UI
            jsonResult.text = result
            Log.d(TAG, "onPostExecute result"+result)
        }

        // runs in background (not in UI thread)
        override fun doInBackground(vararg params: String): String {
            // params comes from the execute() call: params[0] is the url.

            try {
                return downloadData(*params)
            } catch (e: IOException) {
                return "Unable to retrieve web page. URL may be invalid. " + e.message
            }

        }
    } // AsyncTask DownloadFoodData()

    /*
	 * Given a URL, establishes an HttpUrlConnection and retrieves the web page
	 * content as a InputStream, which it returns as a string.
	 */

    @Throws(IOException::class)
    private fun downloadData(vararg params: String): String {
        var instream: InputStream? = null
        val out: OutputStream
        var contentAsString : String?
        val response: Int
        val url: URL
        val urlstring : String

        var conn: HttpsURLConnection? = null

        // this should be moved, creating & parsing the url doesn't need to be done in the background
        //param[0]  URL
        //param[1]  json or get parameter (ingr)
        //param[2]   GET or POST
        //param[3]  appid & key (prepend & if GET as have also ingred"
        val appidkey = params[3]
        val http = params[2]
        var bytes : ByteArray = "".toByteArray(charset("UTF-8"))
        if (http == "GET") {
            urlstring = params[0] + params[1] + "&" + appidkey
        } else {
            urlstring = params[0] + params[3]
            bytes = params[1].toByteArray(charset("UTF-8"))
        }

        try {
            url = URL(urlstring)
            Log.d(TAG, "download type "+http+" url "+url)
        } catch (e: MalformedURLException) {
            Log.d(TAG, e.message.toString())
            return "ERROR call the developer: " + e.message
        }

        try {
            // create and open the connection
            conn = url.openConnection() as HttpsURLConnection

            // output = true, uploading POST data
            // input = true, downloading response to POST
            if (http == "POST")
                conn.doOutput = true

            conn.doInput = true
            // "GET" or "POST"
            conn.requestMethod = http

            // conn.setFixedLengthStreamingMode(params[1].getBytes().length);
            // send body unknown length
            // conn.setChunkedStreamingMode(0);
            conn.readTimeout = 10000
            conn.connectTimeout = 15000
            
            // set length of POST data to send then send it
            if (http == "POST") {
                conn.setRequestProperty("Content-Type",
                    "application/json; charset=UTF-8")
                conn.addRequestProperty("Content-Length", bytes.size.toString())
               
                Log.d(TAG, "post "+bytes.size.toString())
                Log.d(TAG, "post "+bytes)

                //send the POST out
                out = BufferedOutputStream(conn.outputStream)

                out.write(bytes)
                out.flush()
                out.close()
            } else {
                conn.connect()
            }

            // logCertsInfo(conn);

            // now get response
            response = conn.responseCode

            /*
			 *  check the status code HTTP_OK = 200 anything else we didn't get what
			 *  we want in the data.
			 */
            if (response != HttpURLConnection.HTTP_OK) {
                Log.d(TAG, "Server returned: $response aborting read.")
                return "Server returned: $response aborting read."
            }
            instream = conn.inputStream
            contentAsString = readIt(instream)
            Log.d(TAG,contentAsString)
            return contentAsString

        } finally {

            // Make sure that the Reader is closed after the app is finished using it.
            if (instream != null)
                try {
                    instream.close()
                } catch (ignore: IOException) {
                    Log.i(TAG, "instream IOException on close, ignored")
                }

            //* Make sure the connection is closed after the app is finished using it.
            if (conn != null)
                try {

                    conn.disconnect()
                } catch (ignore: IllegalStateException) {
                    Log.i(TAG, "socket IllegalStateException on disconnect, ignored")
                }

        }
    } // downLoadData()

    /*
    * Reads stream from HTTP connection and converts it to a String.
    *
    *  We use a BufferedInputStream to read the data from the http connection InputStream
    *  into our buffer  (NETIOBUFFER bytes at a time, I often choose 1KiB for
    *  probably small datastreams.)
    *
    *  We then use a ByteArrayOutputStream + DataOutputStream.writer to collect the data
    *  and write it to a byte array.
    *  The reason this is done is that we do not know the length of the incoming data
    *  So the OutputStream components allow us to put data into and grow the buffer.
    *
    *  It is unfortunate that in Java everything is a pointer but we do not have access
    *  to the underlying structures.  If this is done in c or c++ we allocate, manage,
    *  grow and release the buffers as we need them.
    *
    *  In java using these classes has that effect.  It is not necessarily efficient,
    *  a better solution may be needed for large data exchange.
    */
    @Throws(IOException::class)
    fun readIt(stream: InputStream?): String {
        var bytesRead: Int
        var totalRead = 0
        val buffer = ByteArray(NETIOBUFFER)

        // for data from the server
        val bufferedInStream = BufferedInputStream(stream!!)
        // to collect data in our output stream
        val byteArrayOutputStream = ByteArrayOutputStream()
        val writer = DataOutputStream(byteArrayOutputStream)

        // read the stream until end
        bytesRead = 0
        while (bytesRead != -1) {
            bytesRead = bufferedInStream.read(buffer)
            if (bytesRead > 0) {
                writer.write(buffer)
                totalRead += bytesRead
            }
        }
        writer.flush()
        Log.d(TAG, "Bytes read: " + totalRead
                + "(-1 means end of reader so max of)")

        return byteArrayOutputStream.toString()
    } // readIt()

    /*
	 * Click Listener for the Show Certs button
	 * It will connect to the website then get the Certificate data to display and log
	 * TODO: This will crash, look at the log to see why, then fix it.
	 */

    fun showCerts(view: View) {
        var url: URL?
        try {
            url = URL(APIURL)
        } catch (e: MalformedURLException) {
            Log.d(TAG, e.message.toString())
            jsonResult.text = "ERROR call the developer: " + e.message
            url = null

        }

        url.let {
            // always trueif (url != null) {
            var conn: HttpsURLConnection? = null

            try {
                conn = url?.openConnection() as HttpsURLConnection
                jsonResult.text = logCertsInfo(conn)
            } catch (e: IOException) {
                Log.d(TAG, e.message.toString())
                jsonResult.text = "ERROR call the developer: " + e.message
            }

            if (conn != null)
                try {
                    //* Make sure the connection is closed after the app is finished using it.
                    conn.disconnect()
                } catch (ignore: IllegalStateException) {
                    Log.i(TAG, "socket IllegalStateException on disconnect, ignore")
                }
        }
    } //showCerts

    private fun logCertsInfo(conn: HttpsURLConnection?): String {
        var line = ""
        if (conn == null)
            return "No connection"
        try {
            line += "Response Code : " + conn.responseCode + "\n"
            Log.d(TAG, "Response Code : " + conn.responseCode)
            line += "Response Code : " + conn.cipherSuite + "\n"
            Log.d(TAG, "Cipher Suite : " + conn.cipherSuite)
            line += "See log for certificates."

            val certs = conn.serverCertificates
            for (cert in certs) {
                Log.d(TAG, "Cert Type : " + cert.type)
                Log.d(TAG, "Cert Hash Code : " + cert.hashCode())
                Log.d(TAG, "Cert Public Key Algorithm : " + cert.publicKey.algorithm)
                Log.d(TAG, "Cert Public Key Format : " + cert.publicKey.format)
            }
            Log.d(TAG, "End Certs")
        } catch (e: SSLPeerUnverifiedException) {
            Log.d(TAG, e.message.toString())
            line += e.message
        } catch (e: IOException) {
            Log.d(TAG, e.message.toString())
            line += e.message
        }

        return line
    } // logCertsInfo()

    companion object {
        private val TAG = "HttpURLPOST"
        private val NETIOBUFFER = 1024
        /*
        * Strings used for the api http://www.edamam.com
        *
        *  n.b. do not hard code an api key like this it should be in a config file
        *
        *  This is my key and my appid
        *  GET YOUR OWN if you will be using this in your app
        *  For the purposes of the lab you can use mine, I think it will not exceed quota.
        */

        /* 2014 key
        private static final String APPKEY = "71b785fe9091261dbe192db334d77dbd";
	    private static final String APPID = "e6501390";
	    */
        /// 2015 key, still good in 2016
        /*
        private val APPKEY = "77397e154a68c54cb1380e268edffa84"
        private val APPID = "b3f7ba07"
        */
        // 2019 food api  GET

        private val APPID="46308b95"
        private val APPKEY="cced96337a51df88ee66f2eb2b9c13c6"
        // 2019 GET
        // https://api.edamam.com/api/food-database/parser?ingr=banana&app_id=${ID}&app_key=${KEY}
        private val APIURL = "https://api.edamam.com/api/food-database/parser?"

        // 2019 nutrition api POST
        private val APPKEYN="d38db7f66e1c6a7b6001dbfb4ea94e28"
        private val APPIDN="501a43e4"
        // 2019 POST
        // https://api.edamam.com/api/nutrition-details?app_id=$IDN&app_key=$KEYN
        private val APIURLN = "https://api.edamam.com/api/nutrition-details?"

        // 2019 example recipe for nutrition api
        val RECIPE1_JSON = """{
  "title": "Fresh Ham Roasted With Rye Bread and Dried Fruit Stuffing",
  "prep": "1. Have your butcher bone and butterfly the ham and score the fat in a diamond pattern. ...",
  "yield": "About 15 servings",
  "ingr": [
    "1 fresh ham, about 18 pounds, prepared by your butcher (See Step 1)",
    "7 cloves garlic, minced",
    "1 tablespoon caraway seeds, crushed",
    "4 teaspoons salt",
    "Freshly ground pepper to taste",
    "1 teaspoon olive oil",
    "1 medium onion, peeled and chopped",
    "3 cups sourdough rye bread, cut into 1/2-inch cubes",
    "1 1/4 cups coarsely chopped pitted prunes",
    "1 1/4 cups coarsely chopped dried apricots",
    "1 large tart apple, peeled, cored and cut into 1/2-inch cubes",
    "2 teaspoons chopped fresh rosemary",
    "1 egg, lightly beaten",
    "1 cup chicken broth, homemade or low-sodium canned"
  ]
}"""

        /*
        used to test JN api on AWS w self signed cert, did not work, untrusted authority

        private val APIURL2 = "https://ec2-34-207-72-182.compute-1.amazonaws.com/api/auth/login"
        private val APIREQjson = "{\"email\":\"j@j\", \"password\":\"1qazxsw2\"}"
        */
    }

} // MainActivity class